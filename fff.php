<?php
 include_once('config.php');
 session_start();
 error_reporting(0);
 ?>

<!DOCTYPE html>
<html>
<head>
  
  <title>Login Page</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/adminmain.css">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
function setIframeSource() {
     var theSelect = document.getElementById('month');
     var theIframe = document.getElementById('myIframe');
     var theUrl;
 
     theUrl = theSelect.options[theSelect.selectedIndex].value;
     theIframe.src = theUrl;
}
</script>

<style type="text/css">
body
{
  margin: 0px;
}
  .mar_down
  {
    margin-top: 100px;
  }

  .month_top
  {
    margin-top: -15px;
  }
  
  .table_brd
  {
    border-style: groove;
    border-color: thin black;
    
  }
  .right_brd
  {
    border-style: groove;
    border-top:none;
    border-left: none;
    border-bottom: none;

  }

</style>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
<div class="container-fluid">
    <div class="row">
    <div id="top_color">
       <div id="wrapping">
            
     
     
       <div id="koan_logo" class="col-xs-2 col-sm-2 col-md-4 col-md-offset-1 col-xs-offset-1 max">
       
         <img src="images/koanlogo.png" class="img-responsive">
          
           </div>

 
          <div class="col-xs-4 col-sm-6 col-md-12 col-md-offset-2 col-sm-offset-3 top-buffer" id="a2"><p><b>KOAN DIGITAL DASHBOARD</b></p>
              </div><!--id="a2" for font-->
             
      
   
   

    <!--dropdownox-->
    <div id="flt" class="pull-right">
       <div class="dropdown">
              
              <div id="but">
             &nbsp;&nbsp;Hi&nbsp;<?php echo $_SESSION['username'];?>&nbsp;&nbsp;
             <!-- <button type="button" class="btn btn-sm"    id="lg" onClick="window.location='logout.php'">
             Logout</button> -->
             </div>
        <button class="btn btn-sm" type="button" id="menu1" data-toggle="dropdown"><!-- <img src="images/icon1.png"/> -->
         <span class="glyphicon glyphicon-th-list"></span>
   <!--      <span class="caret"></span> --></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu2">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="adminmain.php">Home</a></li><br>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="newadminmain.php">Client</a></li><br>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="user.php">Add user</a></li><br>
          
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">About Us</a></li><br>
          <li role="presentation" class="divider"></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="logout.php"><b style="color:purple;"> Logout</b></a></li>
        </ul>
      </div>
      </div>
      




   
   
   </div><!--wrapping-->
   </div><!--top-color-->


   
   
    </div><!--row-->
    
   </div><!-- container -->
   <div class="bor_bottom"></div>
    </div><!--navbar-->
    

                 <!--HEADER PART OVER-->


<div class="container-fluid mar_down">
<div class=" col-md-12 col-sm-7  col-xs-12" >
<form method="post" role="form" name="frm" id="form1">




<div class="row table_brd">
<div class="form-group">
<label for="Date" class="col-md-1 col-xs-4  control-label">Year:<br/>

                           <select name="year" id="year"  method="post">
									<option value="01">2015</option>
									<option value="02">2016</option>
									<option value="03">2017</option>
									<option value="04">2018</option>
									<option value="05">2019</option>
									<option value="06">2020</option>
						 	</select> 
						 	</label>       
</div>

<div class="form-group">
<label for="Date" class="col-md-1 col-xs-5 control-label month_top">
                                                  
                   Month:<br/>
				<select name="month" id="month"  method="post" onchange="setIframeSource()">
							<option>select</option>
							<option value="jan.php">Jan</option>
							<option value="feb.php">Feb</option>
							<option value="mar.php">Mar</option>
							<option value="apr.php">Apr</option>
							<option value="05">May</option>
							<option value="06">June</option>
							<option value="07">July</option>
							<option value="08">Aug</option>
							<option value="09">Sept</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
				</select>
				                    

                  
</label>
</div>
</div>

</form>

<div class="row table_brd1">
<div class="form-group">
<label for="Date" class="col-md-1 control-label text_colr">
	<iframe id="myIframe" src="table.php" frameborder="1" marginwidth="0" marginheight="0" width="1300" height="800"></iframe>
</label>
</div>
</div>


</div>
</div>


 




<!--footer part start-->
          
        <div class="navbar navbar-default navbar-bottom">
          <div class="container-fluid">
              <div class="row">
                  <div class="navbar-btn col-xs-4"><a href="http://koandigital.com/contact/">
                       <span class="glyphicon glyphicon-envelope isize" style="color: rgb(109, 110, 113);">
                        
                       </span></a></div>
            
                     
                
                <div class=" navbar-btn col-xs-4 col-xs-offset-1 col-md-4 col-sm-offset-2">
            <a href="https://www.facebook.com/" ><img src="images/fb_icon.png" width="20px;"></a> </div>
            <div class=" navbar-btn  pull-right">
                <a href="https://www.linkedin.com/uas/login" style="margin-right:20px;"><img src="images/linkedin_icon.png" width="20px;"/></a>
                </div>
             </div>
          </div>
        
       </div> <!--footer-->

        
    
</body>
</html>