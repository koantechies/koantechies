<?php
 session_start();
error_reporting(0);
include "config.php";
if (isset($_SESSION['username'])) 
{
 ?>

<html>
<head>
	<title>Create Client</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/adminmain.css">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>	
</head>
<style type="text/css">
	#user_brder
	{
		margin-top: 130px;
	}
</style>
<body>

<div class="navbar navbar-default navbar-fixed-top">
<div class="container-fluid">
    <div class="row">
    <div id="top_color">
       <div id="wrapping">
            
     
     
       <div id="koan_logo" class="col-xs-2 col-sm-2 col-md-4 col-md-offset-1 col-xs-offset-1 max">
       
         <img src="images/koanlogo.png" class="img-responsive">
          
           </div>

 
          <div class="col-xs-4 col-sm-6 col-md-12 col-md-offset-2 col-sm-offset-3 top-buffer" id="a2"><p><b>KOAN DIGITAL DASHBOARD</b></p>
              </div><!--id="a2" for font-->
             
      
   
   

    <!--dropdownox-->
    <div id="flt" class="pull-right">
       <div class="dropdown">
              
              <div id="but" style="color:purple">
             &nbsp;&nbsp;Hi&nbsp;<?php echo $_SESSION['username'];?>&nbsp;&nbsp;
             <!-- <button type="button" class="btn btn-sm"    id="lg" onClick="window.location='logout.php'">
             Logout</button> -->
             </div>
        <button class="btn btn-sm" type="button" id="menu1" data-toggle="dropdown"><!-- <img src="images/icon1.png"/> -->
         <span class="glyphicon glyphicon-th-list"></span>
   <!--      <span class="caret"></span> --></button>
       <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu2">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="usersdefaultpg.php">Home</a></li><br>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="createintuser.php">Create Internal User</a></li><br>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="createuser.php">Create Client User</a></li><br>

          <li role="presentation"><a role="menuitem" tabindex="-1" href="createclient.php">Create Client</a></li><br>
          
          <li role="presentation"><a role="menuitem" tabindex="-1" href="report2.php">Update Data</a></li><br>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="http://koandigital.com/">About Us</a></li><br>
          <li role="presentation" class="divider"></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="logout.php"><b style="color:purple;"> Logout</b></a></li>
        </ul>
      </div>
      </div>
      




   
   
   </div><!--wrapping-->
   </div><!--top-color-->


   
   
    </div><!--row-->
    
   </div><!-- container -->
   <div class="bor_bottom"></div>
    </div><!--navbar-->
    

                 <!--HEADER PART OVER-->


<div class="container-fluid" id="user_brder">
<div id="table_brd" class="col-md-offset-4 col-md-4 col-sm-7 col-sm-offset-3 col-xs-12" >
<form method="post" name="frm" onSubmit="return client()" enctype="multipart/form-data" action="client_database.php">

<br><br>

<div class="row">
<div class="form-group">
<label for="Name" class="col-xs-5 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Client Name</label>
<div class="text-center"><label for="Name" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
      <input type="text" name="uname" placeholder="Client Name" class="form-control" ></label></div>
 </div>
</div>


<div class="row">
<div class="form-group">
<label for="Address" class="col-xs-5 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Client Address</label>
<div class="text-center"><label for="Address" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
     <input type="text" name="add" placeholder="Client address" class="form-control" ></label></div>
 </div>
</div>

<div class="row">
<div class="form-group">
<label for="unique_id" class="col-xs-5 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Client Id</label>
<div class="text-center"><label for="unique_id" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
     <input type="text" name="uniqueid" placeholder="Unique Id" class="form-control" ></label></div>
 </div>
</div>

<!--<div class="row">
<div class="form-group">
<label for="Mobile" class="col-xs-5 col-sm-4 col-md-6 col-lg-4 col-md-offset-2 control-label text_colr">Mobile</label>
<div class="text-center"><label for="Mobile" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
     <input type="text" name="mob" placeholder="Enter your Mobile Number" class="form-control"></label></div></td></tr>
</div>
</div>

<div class="row">
<div class="form-group">
<label for="Email-Id" class="col-xs-5 col-sm-4 col-md-6 col-lg-4 col-md-offset-2 control-label text_colr">Email-Id</label>
<div class="text-center"><label for="Email-Id" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
             <input type="email" name="email" placeholder="Enter your Valid Email-id" class="form-control"></label></div></td></tr>
</div>
</div>

<div class="row">
<div class="form-group">
<label for="Username" class="col-xs-5 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Username</label>
<div class="text-center"><label for="Username" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
    <input type="text" name="username" placeholder="Enter your UserName"  class="form-control"> </label></div>
 </div>
</div>

<div class="row">
<div class="form-group">
<label for="Password" class="col-xs-5 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Password</label>
<div class="text-center"><label for="Password" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
       <input type="password" name="cpwd" placeholder="Enter your password" class="form-control"></label></div>
 </div>
</div>-->

<div class="row">
<div class="form-group">
<label for="Upload" class="col-xs-5 col-sm-4 col-md-6 col-lg-4 col-md-offset-2 control-label text_colr">Upload-Logo</label>
<div class="text-center"><label for="upload" class="col-xs-9 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
             <input type="File" name="file"   class="form-control"  /></label></div></td></tr>
</div>
</div><br>



<div class="row">
<div class="form-group">

    <div class="col-xs-5 col-sm-4 col-md-4  col-md-offset-2 col-xs-offset-4">
     <input type="submit" name="subc" value="Create Client" class="btn colr form-control">
     
    </div><br><br><br>
 


</div>
</div>
</form>
</div>

</div><!--user create over--><br><br><br><br><br>

<!--footer part start-->
          
        <div class="navbar navbar-default navbar-bottom">
          <div class="container-fluid">
              <div class="row">
                  <div class="navbar-btn col-xs-4"><a href="http://koandigital.com/contact/">
                       <span class="glyphicon glyphicon-envelope isize" style="color: rgb(109, 110, 113);">
                        
                       </span></a></div>
            
                     
                
                <div class=" navbar-btn col-xs-4 col-xs-offset-1 col-md-4 col-sm-offset-2">
            <a href="https://www.facebook.com/" ><img src="images/fb_icon.png" width="20px;"></a> </div>
            <div class=" navbar-btn  pull-right">
                <a href="https://www.linkedin.com/uas/login" style="margin-right:20px;"><img src="images/linkedin_icon.png" width="20px;"/></a>
                </div>
             </div>
          </div>
        
       </div> <!--footer-->

</body>

<script type="text/javascript">
 function client()
 {
  var a = document.frm;
  console.log(a)
  if (a.uname.value== "")
  {
       alert("Please enter the name");
       return false;
  }


  if(!isNaN(a.uname.value)){
    alert("Please enter only characters");
   return false;
  }

  
   var a = document.frm.add.value;

     if (a == '') 
      { 
        alert("Please enter address");
        return false;
      };
     if ((a.length < 10))
      {
        alert("Enter valid address");
        return false;
      };

      var a = document.frm.mob.value;
  if (a == '') 
    {
          alert("Please enter mobile number");
          return false;
    }

    if (isNaN(a)) 
      {
       alert("mobile number is in integer format");
       return false;
      };

    if ((a.length<=9) || (a.length > 10)) 
      {
        alert("mobile number is not valid");
        return false;

      };

    var a = document.frm.email.value;
      if (a == '') 
        {
          alert("Please enter valid email-id");
          return false;
        };
      if (a.indexOf("@",0)<0) 
        {
          alert("please enter '@' function");
          return false;
        };
        if (a.indexOf(".",0)<0) 
        {
          alert("please enter '.' function");
          return false;
        };
}
</script>
</html>

<?php
}
else
 {
  echo "Login again";
  ?>
   <a href="index.php">click here login again</a>
   <?php
}
?>
