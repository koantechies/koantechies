<?php
session_start();
unset($_SESSION['username']);
session_destroy();

header("Location: index.php");
exit;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Logout</title>
</head>
<body>
<a href="index.php">Login again</a>
</body>
</html>