<?php

session_start();
if (isset($_SESSION['username']))
 {
   header('Location: conflict.php');
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Index Page</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/adminmain.css">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
<div class="navbar navbar-default navbar-fixed-top">
<div class="container-fluid">
    <div class="row">
    <div id="top_color">
       <div id="wrapping">
            
     
     
       <div id="koan_logo" class="col-xs-2 col-sm-2 col-md-4 col-md-offset-1 col-xs-offset-1 max">
       
         <img src="images/koanlogo.png" class="img-responsive">
          
           </div>

 
          <div class="col-xs-4 col-sm-6 col-md-12 col-md-offset-3 col-lg-4 col-sm-offset-3 top-buffer" id="a2"><p><b>KOAN DIGITAL DASHBOARD</b></p>
              </div><!--id="a2" for font-->

   </div><!--wrapping-->
   </div><!--top-color-->


   
   
    </div><!--row-->
    
   </div><!-- container -->
   <div class="bor_bottom"></div>
    </div><!--navbar-->

                 <!--HEADER PART OVER-->


<div class="container-fluid" id="index_user">
<div id="table_brd" class="col-md-offset-4 col-md-4 col-sm-7 col-sm-offset-3 col-xs-12" >
<form method="post" role="form" name="frm" onsubmit="return index()" action="newloadr3.php">

<br><br>
<div class="row">
<div class="form-group">
<label for="username" class="col-xs-4 col-sm-4 col-md-8 col-lg-6 col-md-offset-2 control-label text_colr">Username</label>
<div class="text-center"><label for="username" class="col-xs-8 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
<input type="text" class="form-control" id="username" placeholder="Username"  name="username" /></label></div>


</div>
</div>



<div class="row">
<div class="form-group">
<label for="password" class="col-xs-4 col-sm-4 col-md-6 col-lg-4 col-md-offset-2 control-label text_colr">Password</label>
<div class="text-center"><label for="password" class="col-xs-8 col-sm-7 col-md-6 col-lg-7 col-md-offset-2 control-label">
<input type="password" class="form-control" id="password" placeholder="Password" size="30px" name="userpassword" /></label></div></td></tr>
</div>
</div>


<div class="row">
<div class="form-group">
<label class="col-xs-8 col-sm-6 col-md-6 col-lg-7 col-md-offset-2 control-label">
       <a href="forgot.php" >forgot password?</a>
         </label>
<div class="text-center"><label for="password" class="col-xs-8 col-sm-8 col-md-5 col-lg-3 col-md-offset-5 control-label"></label>
</div>
</div>
</div>

<div class="row">
<div class="form-group">

    <div class="col-xs-8 col-sm-8 col-md-6  col-md-offset-2 col-xs-offset-4">
      <input type="submit" class="btn colr" name="pbutton" value="Log in">
    </div><br><br><br>
 


</div>
</div>
</form>
</div>

</div>




  <!--footer part start-->
          </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
          <div class="container-fluid">
              <div class="row">
                  <div class="navbar-btn col-xs-4"><a href="http://koandigital.com/contact/">
                       <span class="glyphicon glyphicon-envelope isize" style="color: rgb(109, 110, 113);">
                        
                       </span></a></div>
            
                     
                
                <div class=" navbar-btn col-xs-4 col-xs-offset-1 col-md-4 col-sm-offset-2">
            <a href="https://www.facebook.com/" ><img src="images/fb_icon.png" width="20px;"></a> </div>
            <div class=" navbar-btn  pull-right">
                <a href="https://www.linkedin.com/uas/login" style="margin-right:20px;"><img src="images/linkedin_icon.png" width="20px;"/></a>
                </div>
             </div>
          </div>
        
       </div> <!--footer-->



                 </div><!--container-fluid-->





<script src="js/jquery-2.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
  function index()
  {
    var a = document.frm;
    if (a.username.value == "") 
      {
        alert("please Enter username");
        return false;
      };
      if (!isNaN(a.username.value))
       {
        alert("Username Must be an Character");
        return false;
       };

     var a = document.frm;
     if (a.userpassword.value == "")
      {
        alert("please enter password");
      };
  }
</script>
</body>
</html>



