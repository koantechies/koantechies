<?php
if (isset($_POST['username'])) {

  $username = $_POST['username'];
  $password = $_POST['userpassword'];

  if (isset($_POST['remember']) && $_POST['remember'] == 'on') {
    /*
     * Set Cookie from here for one hour
     * */
    setcookie("username", $username, time()+(60*60*1));
    setcookie("userpassword", $password, time()+(60*60*1));  /* expire in 1 hour */
  } else {
    /**
     * Following code will unset the cookie
     * it set cookie 1 sec back to current Unix time
     * so that it will invalid
     * */
    //setcookie("username", $username, time()-1);
    //setcookie("password", $password, time()-1);
  }

} else {
  $username = '';
  $password = '';

  if (isset($_COOKIE['username'])) {
    $username = $_COOKIE['username'];
  }

  if (isset($_COOKIE['password'])) {
    $password = $_COOKIE['password'];
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Page</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>

</head>
<style type="text/css">
<!--
#apDiv1 {
	position:absolute;
	width:1338px;
	height:115px;
	z-index:1;
	left: 18px;
	top: 82px;
}
#apDiv2 {
	position:absolute;
	width:1352px;
	height:115px;
	z-index:2;
	left: 12px;
	top: 192px;
}
-->
</style>
</head>

<body>

<div class="container-fluid">
  <div class="row">
    <div id="wrapping"><!--wrapping-->

    <div class="table-responsive">

   <div align="center">  
   
   <div id="top_color">
   <div class="row">
   <div class="col-xs-6 col-sm-6 col-md-12" id="a2">Social Media DashBoard</div>
   
   
     <div id="koan_logo" class="col-xs-6 col-sm-4  col-md-12">
     <!-- <img srcset= src="images/koanlogo.png" 
   sizes="(min-width: 36em) 33.3vw,
      100vw"
   
    /> -->
       <img src="images/koanlogo.png"   style=" width: 115px;margin-right: 107rem; margin-top: -28px; padding-top: 29px;">
       
     </div>
     </div>

   </div>
   <div id="table_bg">
    <table width="200" border="1" >
      <tr>
        <td colspan="2" class="a1">User Login</td>
      </tr>
      <tr>
        <td class="a2">Name</td>
        </tr>
        <tr><td><form id="form1" name="form1" method="post" onsubmit="return form() "action="newloadr3.php">
            <label>
            <input type="text" name="username" id="username" size="30px" />
            </label>
        </td>
      </tr>
      <tr>
        <td class="a2">Password</td>
        </tr>
        <tr><td><label>
          <input type="password" name="userpassword" id="userpassword" size="30px" />
        </label></td>
		
      </tr>
	  <tr>
	  <td>
	  <label><input type="checkbox" name="remember">
            Remember Me on this PC for 1 Hour
          </label>
		  </td>
		  </tr>

      <tr>
        
        <td><label>
          <input type="submit" name="pbutton" id="pbutton" value="Submit" class="a3" />
        </label></td>
      </tr>

       <tr>
        
        <td ><label>
        <a href="http://localhost/koan_office/project1_29/forgot.php" class="a4">forgot password?</a>
          <!-- <input type="button" name="fbutton" id="fbutton" value="forgotpassword" onclick="window.location='forgot.php'" /> -->
        </label></td>
      </tr>

    </table>
    </div><!-- table-bg-->

    </form>
     
</div>

 </div><!-- table-responsive-->
    </div><!-- Wrapping -->
      </div> <!-- row-->
    </div><!-- container -->
    
</body>
</html>
<script type="text/javascript">
  function form()
   {
   var a = document.form1.username.value;
      if (a.length == "") 
        {
          alert("Please Enter username");
          return false;
        }
      if (!isNaN(a))
   
        {
          alert("username must be an character");
          return false;
        }

  var a = document.form1.userpassword.value;
      if (a.length == "") 
        {
          alert("Please Enter your password");
          return false;
        }
    
  }

</script>