-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2015 at 05:47 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `koan29`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(40) NOT NULL,
  `userpassword` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `userpassword`) VALUES
('akshay', 'akshay');

-- --------------------------------------------------------

--
-- Table structure for table `reporting_system`
--

CREATE TABLE IF NOT EXISTS `reporting_system` (
  `Date` varchar(255) NOT NULL,
  `b` int(11) NOT NULL,
  `Spent` float NOT NULL,
  `Impressions` float NOT NULL,
  `Clicks` int(11) NOT NULL,
  `Web_sessions` int(11) NOT NULL,
  `M_sessions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reporting_system`
--

INSERT INTO `reporting_system` (`Date`, `b`, `Spent`, `Impressions`, `Clicks`, `Web_sessions`, `M_sessions`) VALUES
('02/12/2014', 0, 23, 78, 34, 34, 56),
('12/12/12', 0, 10.1, 10.9, 100, 11, 100),
('12/12/2014', 0, 12.12, 12.14, 1, 7, 1),
('14/2/2014', 0, 2.6, 8, 99, 66, 88),
('22/3/2014', 0, 3.3, 6.9, 77, 99, 88),
('22/4/2014', 0, 2.6, 5.5, 4, 8, 55);

-- --------------------------------------------------------

--
-- Table structure for table `sighn`
--

CREATE TABLE IF NOT EXISTS `sighn` (
  `Email` varchar(40) NOT NULL,
  `User_Name` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(40) NOT NULL,
  `userpassword` varchar(40) NOT NULL,
  `Email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `userpassword`, `Email`) VALUES
('akshay', 'akshay', 'akshay.anuradha@gmail.com'),
('ambi', 'ambi', 'akshay.nanjundasmailbox@rediffmail.com'),
('', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reporting_system`
--
ALTER TABLE `reporting_system`
 ADD PRIMARY KEY (`Date`);

--
-- Indexes for table `sighn`
--
ALTER TABLE `sighn`
 ADD PRIMARY KEY (`Email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
